#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4443

struct account{
	char username[1000];
	char password[1000];
};

int check(char *username, char *password){
	FILE *fp;
	struct account user;
	int id, found = 0;

    // open users data
	fp = fopen("../database/databases/user.dat", "rb");

	while(1){	
		fread(&user, sizeof(user), 1, fp);

        // if username registered
		if(strcmp(user.username, username)==0){
			// if password matched
			if(strcmp(user.password, password)==0) found = 1;
		}
		if(feof(fp)) break;
	}
	fclose(fp);

	if(found==1){
        printf("Logged in.\n");
		return 1;
	} else{
        printf("Invalid username or password.\n");
		return 0;
	}
	
}

void writelog(char *command, char *user){
	time_t rawtime;
	struct tm *timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	FILE *fp;
	char loc[1000], info[1000];

	snprintf(loc, sizeof loc, "../database/log/log%s.log", user);
	fp = fopen(loc, "ab");

	sprintf(info, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, user, command);
	fputs(info, fp);
	fclose(fp);
	return;
}

int main(int argc, char *argv[]){
	int allowed = 0;
	int id_user = geteuid();
	char database_used[1000];

	if(geteuid() == 0){
        // root
		allowed = 1;
	} else{
		int id = geteuid();
		allowed = check(argv[2], argv[4]);
	}

	if(allowed==0){
		return 0;
	}

	int clientSocket, ret;
	struct sockaddr_in serverAddr;
	char buffer[32768];

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(clientSocket < 0){
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Client Socket is created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Connected to Server.\n");
    
	while(1){
		char input[1000], input2[1000];
		char command[100][1000];
		char *token;
		int i=0;  

        printf("Client: \t");
		scanf(" %[^\n]s", input);
		strcpy(input2, input);
		token = strtok(input, " ");

		while(token != NULL) {
			strcpy(command[i], token);
			token = strtok(NULL, " ");
            i++;
		}

		int wrong = 0;
		if(strcmp(command[0], "CREATE")==0){
			if(strcmp(command[1], "USER")==0 && strcmp(command[3], "IDENTIFIED")==0 && strcmp(command[4], "BY")==0){
				snprintf(buffer, sizeof buffer, "cUser:%s:%s:%d", command[2], command[5], id_user);
				send(clientSocket, buffer, strlen(buffer), 0);
			} else if(strcmp(command[1], "DATABASE")==0){
				snprintf(buffer, sizeof buffer, "cDatabase:%s:%s:%d", command[2], argv[2], id_user);
				send(clientSocket, buffer, strlen(buffer), 0);
			} else if(strcmp(command[1], "TABLE")==0){
				snprintf(buffer, sizeof buffer, "cTable:%s", input2);
				send(clientSocket, buffer, strlen(buffer), 0);
			}
		} else if(strcmp(command[0], "GRANT")==0 && strcmp(command[1], "PERMISSION")==0 && strcmp(command[3], "INTO")==0){
			snprintf(buffer, sizeof buffer, "gPermission:%s:%s:%d", command[2],command[4], id_user);
			send(clientSocket, buffer, strlen(buffer), 0);
		} else if(strcmp(command[0], "USE")==0){
			snprintf(buffer, sizeof buffer, "uDatabase:%s:%s:%d", command[1], argv[2], id_user);
			send(clientSocket, buffer, strlen(buffer), 0);
		} else if(strcmp(command[0], "cekCurrentDatabase")==0){
			snprintf(buffer, sizeof buffer, "%s", command[0]);
			send(clientSocket, buffer, strlen(buffer), 0);
		} else if(strcmp(command[0], "DROP")==0){
			if(strcmp(command[1], "DATABASE")==0){
				snprintf(buffer, sizeof buffer, "dDatabase:%s:%s", command[2], argv[2]);
				send(clientSocket, buffer, strlen(buffer), 0);
			} else if(strcmp(command[1], "TABLE")==0){
				snprintf(buffer, sizeof buffer, "dTable:%s:%s", command[2], argv[2]);
				send(clientSocket, buffer, strlen(buffer), 0);
			} else if(strcmp(command[1], "COLUMN")==0){
				snprintf(buffer, sizeof buffer, "dColumn:%s:%s:%s", command[2], command[4] ,argv[2]);
				send(clientSocket, buffer, strlen(buffer), 0);
			}
		} else if(strcmp(command[0], "INSERT")==0 && strcmp(command[1], "INTO")==0){
            snprintf(buffer, sizeof buffer, "insert:%s", input2);
			send(clientSocket, buffer, strlen(buffer), 0);
        } else if(strcmp(command[0], "UPDATE")==0){
            snprintf(buffer, sizeof buffer, "update:%s", input2);
			send(clientSocket, buffer, strlen(buffer), 0);
        } else if(strcmp(command[0], "DELETE")==0){
            snprintf(buffer, sizeof buffer, "delete:%s", input2);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if(strcmp(command[0], "SELECT")==0){
            snprintf(buffer, sizeof buffer, "select:%s", input2);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if(strcmp(command[0], ":exit")!=0){
			wrong = 1;
			char warning[] = "Wrong Command";
			send(clientSocket, warning, strlen(warning), 0);
		}

		if(wrong != 1){
			char usn[10000];
			if(id_user == 0){
				strcpy(usn, "root");
			} else{
				strcpy(usn, argv[2]);
			}
			writelog(input2, usn);
		}

		if(strcmp(command[0], ":exit") == 0){
			send(clientSocket, command[0], strlen(command[0]), 0);
			close(clientSocket);
			printf("[-]Disconnected from server.\n");
			exit(1);
		}
		bzero(buffer, sizeof(buffer));

		if(recv(clientSocket, buffer, 1024, 0) < 0){
			printf("[-]Error in receiving data.\n");
		} else{
			printf("Server: \t%s\n", buffer);
		}
	}

	return 0;
}
